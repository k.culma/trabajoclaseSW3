﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace trabajo_keyla
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de clase "Service1" en el código, en svc y en el archivo de configuración.
    // NOTE: para iniciar el Cliente de prueba WCF para probar este servicio, seleccione Service1.svc o Service1.svc.cs en el Explorador de soluciones e inicie la depuración.
    public class Service1 : IService1
    {
        private Model1Container db = new Model1Container();

        public List<empresa> mostrarempresas()
        {
            return db.empresaSet.ToList();

        }

        public empresa mostrarunaempresa(string Id)
        {
            int nId = Convert.ToInt16(Id);
            return db.empresaSet.Where(p => p.Id == nId).First();
        }


        public List<tipoempresa> mostrartipoempresa()
        {
            return db.tipoempresaSet.ToList();

        }
    }
}
