
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 07/18/2015 00:14:45
-- Generated from EDMX file: C:\Users\DAVID\Desktop\sw3\trabajo_keyla\trabajo_keyla\Model1.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [master];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------


-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------


-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'empresaSet'
CREATE TABLE [dbo].[empresaSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [nombre] nvarchar(max)  NOT NULL,
    [direccion] nvarchar(max)  NOT NULL,
    [telefono] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'tipoempresaSet'
CREATE TABLE [dbo].[tipoempresaSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [nombre_tipo] nvarchar(max)  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'empresaSet'
ALTER TABLE [dbo].[empresaSet]
ADD CONSTRAINT [PK_empresaSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'tipoempresaSet'
ALTER TABLE [dbo].[tipoempresaSet]
ADD CONSTRAINT [PK_tipoempresaSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------