﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace trabajo_keyla
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de interfaz "IService1" en el código y en el archivo de configuración a la vez.
    [ServiceContract]
    public interface IService1
    {

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "mostrarempresa", ResponseFormat = WebMessageFormat.Json)]
        List<empresa> mostrarempresas();


        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "mostrarunaempresa", ResponseFormat = WebMessageFormat.Json)]
        empresa mostrarunaempresa(string Id);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "mostrartipoempresa", ResponseFormat = WebMessageFormat.Json)]
        empresa mostrartipoempresa();
        
    }

}
